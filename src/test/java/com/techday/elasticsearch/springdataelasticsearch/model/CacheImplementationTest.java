package com.techday.elasticsearch.springdataelasticsearch.model;

import org.junit.Assert;
import org.junit.Test;

public class CacheImplementationTest {

    @Test
    public void chechCache() {

        final CacheImplementation<String, String> cache = new CacheImplementation<>(3);
        cache.put("First","1");
        cache.put("Second","2");
        cache.put("Third","3");
        cache.get("First");
        cache.put("4","4");

        final String first = cache.get("Second");
        Assert.assertNull(first);
    }

    @Test
    public void checkRemove() {
        final CacheImplementation<String, String> cache = new CacheImplementation<>(3);
        cache.put("First","1");
        cache.put("Second","2");
        cache.put("Third","3");
        cache.get("Something");
        cache.put("4","4");

        final String first = cache.get("First");
        Assert.assertNull(first);

        cache.get("D1");
        cache.get("D2");
        cache.get("D3");
        cache.put("4","4");
        Assert.assertNotNull(cache.get("Second"));
        Assert.assertNotNull(cache.get("Third"));
        Assert.assertNotNull(cache.get("4"));


    }
}