package com.techday.elasticsearch.springdataelasticsearch.model;

import org.junit.Assert;
import org.junit.Test;

public class PriceCalculatorServiceTest {

    @Test
    public void getDiscountedPrice_GivenAProductItem_shouldHaveDiscountedPrice() {

        final Product product = Product.builder().name("P 1").price(20l).build();
        final ProductItem productItem = ProductItem.builder().product(product).quantity(1).build();
        final PriceCalculatorService priceCalculatorService = new PriceCalculatorService();
        final long discountedPriceItem = priceCalculatorService.getDiscountedPriceItem(productItem, 0l);
        Assert.assertEquals(20l, discountedPriceItem);

    }

}