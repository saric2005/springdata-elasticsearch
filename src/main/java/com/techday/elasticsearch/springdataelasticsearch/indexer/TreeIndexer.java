package com.techday.elasticsearch.springdataelasticsearch.indexer;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;


import com.techday.elasticsearch.springdataelasticsearch.model.NewyorkTree;
import com.techday.elasticsearch.springdataelasticsearch.model.Tree;
import com.techday.elasticsearch.springdataelasticsearch.repository.TreeRepository;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Component
public class TreeIndexer {

    private final RestTemplate restTemplate;
    private final TreeRepository treeRepository;

    public void index() {
        NewyorkTree[] newyorkTrees = restTemplate.getForObject("https://data.cityofnewyork.us/resource/nwxe-4ae8.json", NewyorkTree[].class);
        final List<Tree> treeList = Arrays.asList(newyorkTrees).stream().map(NewyorkTree::toTree).collect(Collectors.toList());
        treeRepository.refresh();
        treeRepository.saveAll(treeList);
    }
}
