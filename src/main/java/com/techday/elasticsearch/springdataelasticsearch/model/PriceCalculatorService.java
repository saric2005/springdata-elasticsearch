package com.techday.elasticsearch.springdataelasticsearch.model;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Service;

@Service
public class PriceCalculatorService {

    public Long getPriceForOrder(@NotNull Order order) {
        return getTicketPrice(order.getTickets());
    }

    private Long getTicketPrice(List<Ticket> tickets) {
        Long totalPrice = 0l;
        for (Ticket ticket : tickets) {
            totalPrice = totalPrice + getProductItemPrice(ticket.getProductItems(), ticket.getDiscount());
            totalPrice = totalPrice + ticket.getPrice() * (100 - ticket.getDiscount()) / 100;
        }
        return totalPrice;
    }


    private Long getProductItemPrice(@NotNull List<ProductItem> productItems, Long discount) {
        Long totalPrice = 0l;

        for (ProductItem item : productItems) {
            totalPrice = totalPrice + getDiscountedPriceItem(item, discount);
        }
        return totalPrice;
    }

    protected long getDiscountedPriceItem(ProductItem item, Long discount) {
        return (item.getProduct().getPrice() * (100 - discount) / 100) * item.getQuantity();
    }

}
