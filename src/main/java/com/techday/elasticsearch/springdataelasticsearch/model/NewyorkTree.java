package com.techday.elasticsearch.springdataelasticsearch.model;

import org.springframework.data.elasticsearch.core.geo.GeoPoint;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Data
public class NewyorkTree {
    @JsonProperty("tree_id")
    private Long treeId;

    @JsonProperty("spc_common")
    private String spcCommon;

    private double latitude;

    private double longitude;

    public Tree toTree() {
        return Tree.builder().spcCommon(spcCommon).treeId(treeId).location(new GeoPoint(latitude, longitude)).build();
    }
}
