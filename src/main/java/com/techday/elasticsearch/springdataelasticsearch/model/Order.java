package com.techday.elasticsearch.springdataelasticsearch.model;

import java.util.List;

import lombok.Data;

@Data
public class Order {
    private List<Ticket> tickets;
}
