package com.techday.elasticsearch.springdataelasticsearch.model;

import static org.springframework.data.elasticsearch.annotations.FieldType.Text;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.InnerField;
import org.springframework.data.elasticsearch.annotations.MultiField;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

@Document(indexName = "health-check", type = "tree")
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
@Data
public class Tree {
    @Tolerate
    public Tree() {
    }

    @Id
    private Long treeId;

    @MultiField(mainField = @Field(type = Text),
            otherFields = { @InnerField(suffix = "untouched", type = Text, store = true, fielddata = true, analyzer = "keyword") })
    private String spcCommon;
    private GeoPoint location;
}
