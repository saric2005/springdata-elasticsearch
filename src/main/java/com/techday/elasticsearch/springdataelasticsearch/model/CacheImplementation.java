package com.techday.elasticsearch.springdataelasticsearch.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.google.common.collect.Lists;

public class CacheImplementation<K, V> implements Cache<K, V> {

    private Map<K, V> cache;
    private int capacity;
    private List<K> recentUpdates;

    public CacheImplementation(int capacity) {
        this.capacity = capacity;
        this.cache = new HashMap<>();
        this.recentUpdates = Lists.newArrayList();
    }


    @Override
    public void clear() {
        recentUpdates.clear();
        cache.clear();
    }

    @Override
    public V get(K key) {
        final boolean remove = recentUpdates.remove(key);
        if (remove) {
            recentUpdates.add(key);
        }
        return cache.get(key);
    }

    @Override
    public void put(K key, V value) {
        if (!cache.containsKey(key) && cache.size() == this.capacity) {
            removeUnusedOne();
        }
        cache.put(key, value);
        recentUpdates.remove(key);
        recentUpdates.add(key);
    }

    private void removeUnusedOne() {
        final K lastUsed = recentUpdates.get(0);
        cache.remove(lastUsed);
        recentUpdates.remove(0);
    }

    @Override
    public boolean remove(K key) {
        recentUpdates.remove(key);
        return Optional.ofNullable(cache.remove(key)).isPresent();
    }

    @Override
    public int size() {
        return this.cache.size();
    }

    @Override
    public int capacity() {
        return this.capacity;
    }
}
