package com.techday.elasticsearch.springdataelasticsearch.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProductItem {
    private Product product;
    private Integer quantity;
}
