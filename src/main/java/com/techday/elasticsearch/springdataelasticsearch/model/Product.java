package com.techday.elasticsearch.springdataelasticsearch.model;

import org.springframework.data.annotation.Id;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Product {
    private String name;
    private Long price;

}

