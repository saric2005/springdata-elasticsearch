package com.techday.elasticsearch.springdataelasticsearch.model;

import java.util.List;

import lombok.Data;

@Data
public class Ticket {
    private List<ProductItem> productItems;
    private Long price;
    private Long discount;
    // extra infos
}
