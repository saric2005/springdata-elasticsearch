package com.techday.elasticsearch.springdataelasticsearch.service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.common.unit.DistanceUnit;
import org.elasticsearch.index.query.GeoDistanceQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.InternalTerms;
import org.elasticsearch.search.aggregations.bucket.terms.StringTerms;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;


import com.techday.elasticsearch.springdataelasticsearch.model.Tree;
import com.techday.elasticsearch.springdataelasticsearch.repository.TreeRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class TreeService {

    private static final String INDEX = "health-check";
    private static final String DOCUMENT_TYPE = "tree";
    public static final String SPC_COMMON_UNTOUCHED = "spcCommon.untouched";
    public static final String SPC_COMMONS = "spcCommons";
    public static final String LOCATION = "location";
    public static final String UNIT = "m";
    private final TreeRepository treeRepository;
    private final ElasticsearchOperations elasticsearchOperations;

    public List<Tree> findResourceByLocation(double lat, double longitute, double radius, int offset, int size) {
        GeoPoint startLocation = new GeoPoint(lat, longitute);
        final Page<Tree> byLocationWithin = treeRepository
                .findByLocationWithin(startLocation, radius + UNIT, PageRequest.of(offset, size));
        return byLocationWithin.getContent();
    }

    public Map<String, Long> findResourceByLocationGrouped(double lat, double longitute, double radius) {

        final GeoDistanceQueryBuilder locationQuery = QueryBuilders.geoDistanceQuery(LOCATION).point(lat, longitute).distance(radius, DistanceUnit.METERS);
        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(locationQuery)
                .withSearchType(SearchType.DEFAULT)
                .withIndices(INDEX).withTypes(DOCUMENT_TYPE)
                .addAggregation(AggregationBuilders.terms(SPC_COMMONS).field(SPC_COMMON_UNTOUCHED).size(50))
                .build();

        return elasticsearchOperations.query(searchQuery, response -> {
            final List<StringTerms.Bucket> groupedCount = ((StringTerms) response.getAggregations().asMap().get(SPC_COMMONS)).getBuckets();
            return groupedCount
                    .stream()
                    .collect(Collectors.toMap(StringTerms.Bucket::getKeyAsString, InternalTerms.Bucket::getDocCount));
        });

    }
}
