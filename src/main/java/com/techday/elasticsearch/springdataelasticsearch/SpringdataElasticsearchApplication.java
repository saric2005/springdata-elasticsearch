package com.techday.elasticsearch.springdataelasticsearch;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import com.techday.elasticsearch.springdataelasticsearch.indexer.TreeIndexer;

@SpringBootApplication
public class SpringdataElasticsearchApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringdataElasticsearchApplication.class, args);
    }

    @Bean
    public CommandLineRunner indexTree(TreeIndexer treeIndexer) {
        return (args) -> treeIndexer.index();
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

}

