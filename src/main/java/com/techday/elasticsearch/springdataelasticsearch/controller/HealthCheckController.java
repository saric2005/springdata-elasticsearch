package com.techday.elasticsearch.springdataelasticsearch.controller;

import java.util.List;
import java.util.Map;

import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.Preconditions;
import com.techday.elasticsearch.springdataelasticsearch.indexer.TreeIndexer;
import com.techday.elasticsearch.springdataelasticsearch.model.Tree;
import com.techday.elasticsearch.springdataelasticsearch.service.TreeService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/health-check")
@RequiredArgsConstructor
public class HealthCheckController {

    private final TreeService treeService;
    private final TreeIndexer treeIndexer;

    @NonNull
    @GetMapping("/find-tree/{lat}/{longitute}/{radius}/{offset}/{size}")
    public List<Tree> findResourceByLocation(@PathVariable double lat,
                                             @PathVariable double longitute,
                                             @PathVariable double radius,
                                             @PathVariable int offset,
                                             @PathVariable int size) {
        Preconditions.checkArgument(offset >= 0, "Offset cannot be less than zero");
        Preconditions.checkArgument(size >= 0, "Size cannot be less than zero");
        Preconditions.checkArgument(radius >= 0, "Radius cannot be less than zero");
        return treeService.findResourceByLocation(lat, longitute, radius, offset, size);
    }

    @NonNull
    @GetMapping("/find-grouped-tree/{lat}/{longitute}/{radius}")
    public Map<String, Long> findResourceByLocationGrouped(@PathVariable double lat,
                                                           @PathVariable double longitute,
                                                           @PathVariable double radius) {
        Preconditions.checkArgument(radius >= 0, "Radius cannot be less than zero");
        return treeService.findResourceByLocationGrouped(lat, longitute, radius);
    }

    @PutMapping("/index")
    public void indexTree() {
        treeIndexer.index();
    }

}
