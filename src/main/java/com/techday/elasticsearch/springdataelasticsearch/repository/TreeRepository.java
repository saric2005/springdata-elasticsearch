package com.techday.elasticsearch.springdataelasticsearch.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import com.techday.elasticsearch.springdataelasticsearch.model.Tree;

@Repository
public interface TreeRepository extends ElasticsearchRepository<Tree, Long> {
    Page<Tree> findByLocationWithin(GeoPoint point, String distance, Pageable pageable);
    Page<Tree> findBySpcCommon(String string);
}
